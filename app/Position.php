<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
  protected $fillable = [
      'name', 'election_id',
  ];

  public function candidates()
  {
    return $this->hasMany('App\Candidate');
  }

  public function election()
  {
    return $this->belongsTo('App\Election');
  }
}

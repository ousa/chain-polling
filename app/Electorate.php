<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Electorate extends Model
{
  protected $fillable = [

  ];

  public function card()
  {
    return $this->hasOne('App\Card');
  }

  public function votes()
  {
    return $this->hasMany('App\Vote');
  }
}

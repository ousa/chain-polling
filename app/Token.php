<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
  protected $fillable = [
      'token', 'referee_id',
  ];

  public function referee()
  {
    return $this->belongsTo('App\Referee');
  }
}

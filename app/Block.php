<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
  protected $fillable = [
      'data', 'hash', 'previous_id',
  ];

  public function previous()
  {
    return $this->belongsTo('App\Block');
  }
}

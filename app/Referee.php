<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referee extends Model
{
  protected $fillable = [
      'user_id', 'user_password', 'election_id', 'active',
  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function election()
  {
    return $this->belongsTo('App\Election');
  }

  public function tokens()
  {
    return $this->hasMany('App\Token');
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth;

use App\Election;

class OpenController extends Controller
{
    public function index()
    {
      return view('welcome', [
        'elections' => Election::where('close', '>', Carbon::now())->get(),
      ]);
    }
}

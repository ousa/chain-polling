<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
  protected $fillable = [
      'candidate_id', 'electorate_id',
  ];

  public function candidate()
  {
    return $this->belongsTo('App\User');
  }

  public function electorate()
  {
    return $this->belongsTo('App\Electorate');
  }
}

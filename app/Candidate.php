<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
  protected $fillable = [
      'name', 'description', 'img', 'position_id',
  ];

  public function position()
  {
    return $this->belongsTo('App\Position');
  }

  public function votes()
  {
    return $this->hasMany('App\Vote');
  }
}

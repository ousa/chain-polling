<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function elections()
    {
      return $this->hasMany('App\Election');
    }

    public function nominations()
    {
      return $this->hasMany('App\Nomination');
    }

    public function connections()
    {
      return $this->hasMany('App\Connection');
    }

    public function referees()
    {
      return $this->hasMany('App\Referee');
    }
}

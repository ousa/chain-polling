<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  protected $fillable = [
      'token_id', 'electorate_id',
  ];

  public function token()
  {
    return $this->belongsTo('App\Token');
  }

  public function electorate()
  {
    return $this->belongsTo('App\Electorate');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
  protected $fillable = [
      'email', 'election_id',
  ];

  public function election()
  {
    return $this->belongsTo('App\Election');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
  protected $fillable = [
      'name', 'code', 'open', 'close', 'user_id',
  ];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function positions()
  {
    return $this->hasMany('App\Position');
  }

  public function candidates()
  {
    return $this->hasManyThrough('App\Candidate', 'App\Position');
  }

  public function nodes()
  {
    return $this->hasMany('App\Node');
  }

  public function referees()
  {
    return $this->hasMany('App\Referee');
  }

  public function nominations()
  {
    return $this->hasMany('App\Nomination');
  }

  public function cards()
  {
    return $this->hasMany('App\Card');
  }

  public function votes()
  {
    return $this->hasManyThrough('App\Vote', 'App\Candidate');
  }
}

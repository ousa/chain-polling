@if (session('status'))
<?php
	$_type = 'success';
	if (session('status_type')) {
		$_type = session('status_type');
	}
 ?>
 	<script src="{{ asset('theme/js/bootstrap-notify.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.notify({
				icon: 'ti-info-alt',
				message: "{{ session('status') }}"
			},{
				type: '{{ $_type }}',
				timer: 5000,
				placement: {
					from: 'top',
					align: 'center'
				}
			});
		});
	</script>
@endif

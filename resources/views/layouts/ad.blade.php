<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('theme/img/favicon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>{{ config('app.name') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="{{ asset('theme/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{ asset('theme/css/paper-dashboard.css') }}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('theme/css/demo.css') }}" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('theme/css/themify-icons.css') }}" rel="stylesheet">

</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="black" data-active-color="danger">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{ url('/') }}" class="simple-text">
									<span class="text-danger">{{ config('app.name') }}</span>
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{ route('home') }}">
                        <i class="ti-menu"></i>
                        <p>Home</p>
                    </a>
                </li>
								<li>
                    <a href="">
                        <i class="ti-direction-alt"></i>
                        <p>Elections</p>
                    </a>
                </li>
								<li>
                    <a href="">
                        <i class="ti-bar-chart"></i>
                        <p>Results</p>
                    </a>
                </li>
								@include('layouts.inc.blocks')
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">
											@isset($title)
												{{ $title }}
											@else
												Fair Politics
											@endif
										</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
											@guest
													<li>
														<a href="{{ route('login') }}">
															Login
														</a>
													</li>
													<li>
														<a href="{{ route('register') }}">
															Register
														</a>
													</li>
											@else
													<li class="dropdown">
															<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
																<i class="ti-user"></i>	{{ Auth::user()->name }} <span class="caret"></span>
															</a>
															<ul class="dropdown-menu">
																	<li>
																			<a href="{{ route('logout') }}"
																					onclick="event.preventDefault();
																									 document.getElementById('logout-form').submit();">
																					Logout
																			</a>

																			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
																					{{ csrf_field() }}
																			</form>
																	</li>
															</ul>
													</li>
											@endguest
                    	</ul>
                </div>
            </div>
        </nav>

		<div class="content">
      <div class="container-fluid">
        @yield('content')
			</div>
		</div>

		<footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                  <ul>
                      <li>
                          <a href="">
                              Help
                          </a>
                      </li>
                  </ul>
                </nav>
				<div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="ti-heart"></i> by <a href="http://www.itca.utg.edu.gm" target="_blank">ITCA</a>
                </div>
            </div>
        </footer>

    </div>
</div>
</body>

  <!--   Core JS Files   -->
  <script src="{{ asset('theme/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
	<script src="{{ asset('theme/js/bootstrap.min.js') }}" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{ asset('theme/js/bootstrap- checkbox-radio.js') }}"></script>
	<!--  Charts Plugin -->
	<script src="{{ asset('theme/js/chartist.min.js') }}"></script>
	<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="{{ asset('theme/js/paper-dashboard.js') }}"></script>
	@include('layouts.notification')
</html>

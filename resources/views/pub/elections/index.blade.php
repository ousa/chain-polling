@extends('layouts.pub')

@section('content')
<div class="card">
  <div class="header">
    <h4 class="title">Dashboard</h4>
  </div>
  <div class="content table-responsive table-full-width">
      <table class="table table-striped">
          <thead>
              <tr>
              	<th>Code</th>
              	<th>Name</th>
              	<th>Opening</th>
              	<th>Closing</th>
              </tr>
          </thead>
          <tbody class="table-hover">
            @foreach($elections as $election)
              <tr>
              	<th>
                  <a href="#">{{ $election->code }}</a>
                </th>
                <th>{{ $election->name }}</th>
              	<td>{{ $election->open }}</td>
              	<td>{{ $election->close }}</td>
              </tr>
            @endforeach
          </tbody>
      </table>
  </div>
</div>
@endsection

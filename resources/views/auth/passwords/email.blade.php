@extends('layouts.pub')

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="card">
      <div class="header">
        <h4 class="title">Reset Password</h4>
      </div>
      <div class="content">
        <form method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label>E-mail Address</label>
                <input id="email" type="email" class="form-control border-input" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                  <strong class="text-danger">{{ $errors->first('email') }}</strong>
                @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-fill">
                  Send Password Reset Link
              </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

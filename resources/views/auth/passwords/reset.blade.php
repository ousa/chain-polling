@extends('layouts.pub')

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="card">
      <div class="header">
        <h4 class="title">Reset Password</h4>
      </div>
      <div class="content">
        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label>E-mail Address</label>
                <input id="email" type="email" class="form-control border-input" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                  <strong class="text-danger">{{ $errors->first('email') }}</strong>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label>Password</label>
                <input id="password" type="password" class="form-control border-input" name="password" required>
                @if ($errors->has('password'))
                  <strong class="text-danger">{{ $errors->first('password') }}</strong>
                @endif
            </div>

            <div class="form-group">
              <label>Confirm Password</label>
              <input id="password-confirm" type="password" class="form-control border-input" name="password_confirmation" required>
              @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-fill btn-block">
                  Reset Password
              </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@extends('layouts.pub')

@section('content')
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="card">
      <div class="header">
        <h4 class="title">Login</h4>
      </div>
      <div class="content">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label>E-Mail Address</label>
              <input id="email" type="email" class="form-control border-input" name="email" value="{{ old('email') }}" required autofocus>

              @if ($errors->has('email'))
                <strong class="text-danger">{{ $errors->first('email') }}</strong>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label>Password</label>
              <input id="password" type="password" class="form-control border-input" name="password" required>

              @if ($errors->has('password'))
                <strong class="text-danger">{{ $errors->first('password') }}</strong>
              @endif
            </div>

            <div class="form-group">
              <div class="form-control">
                  <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                  </label>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-fill btn-block">
                  Login
              </button>
              <a class="btn btn-simple" href="{{ route('password.request') }}">
                  Forgot Your Password?
              </a>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
